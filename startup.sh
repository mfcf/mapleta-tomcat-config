#!/bin/sh
cd ~tomcat
. ./.profile
CATALINA_BASE=$CATALINA_HOME
/usr/bin/Xvfb :1 -screen 0 1024x768x8 &
export DISPLAY=:1
./bin/jsvc -user tomcat \
    -cp $CATALINA_HOME/bin/bootstrap.jar:$CATALINA_HOME/bin/tomcat-juli.jar \
    -outfile $CATALINA_BASE/logs/catalina.out \
    -errfile $CATALINA_BASE/logs/catalina.err \
    -Dcatalina.home=$CATALINA_HOME \
    -Dcatalina.base=$CATALINA_BASE \
    -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager \
    -Djava.util.logging.config.file=$CATALINA_BASE/conf/logging.properties \
    -Dfile.encoding=UTF-8 \
    -Duser.language=en \
    -Djava.library.path=/usr/local/apr/lib \
    -Xms128M -Xmx4096M -XX:PermSize=64M -XX:MaxPermSize=128M \
    org.apache.catalina.startup.Bootstrap
