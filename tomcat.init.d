#!/bin/bash
#
# chkconfig: 2345 85 15
# description: Generic start and stop script

# Source function library.
if [ -f /etc/rc.d/functions ];
then
    . /etc/init.d/functions
fi

WHATEVER="Tomcat"
PIDFILE="/var/run/jsvc.pid"

start()
{
	echo $"Starting ${WHATEVER}: "
	~tomcat/startup.sh
	
	if [ $? -eq 0 ];
	then
		echo "${WHATEVER} started successfully"
                exit 0
	else
		echo "${WHATEVER} NOT STARTED"
                exit 1
	fi
}

stop()
{
	echo $"Stopping ${WHATEVER}: "
	~tomcat/shutdown.sh
}

# See how we were called.
case "$1" in
  start)
        start
        ;;
  stop)
        stop
        ;;
  restart|reload)
        stop
        sleep 3
        start
        ;;
  condrestart)
        if [ -f "${PIDFILE}" ]; then
            stop
            sleep 3
            start
        fi
        ;;
  status)
        if [ -f "${PIDFILE}" ]; then
            ps -p `cat ${PIDFILE}` > /dev/null
            if [ $? -eq 0 ]; then
                echo $"`cat ${PIDFILE}`: ${WHATEVER} is running"
            else
                echo $"${WHATEVER} has a pid file, but no matching process"
            fi
        else
            echo $"${WHATEVER} is stopped"
        fi
        ;;
  *)
        echo $"Usage: $0 {start|stop|restart|condrestart|status}"
        exit 1
esac

